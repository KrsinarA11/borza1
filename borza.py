import urllib.parse
import urllib.request
import http.cookiejar
import re
from tkinter import *
import random

cj = http.cookiejar.CookieJar()
opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agent:', 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)')]
urllib.request.install_opener(opener)

url1 = 'http://www.ljse.si/cgi-bin/jve.cgi?doc=7017&onepage=1'

req1 = urllib.request.Request(url1)
response1 = urllib.request.urlopen(req1)
the_page1 = response1.read().decode('cp1250')

with open('podatki.txt', 'w') as g:
     print(the_page1, file=g)

ime1 = re.findall(r'>'+r'\w*'+r'</a>', open('podatki.txt', 'r', encoding='cp1250').read())
seznam1 = []
for i in ime1:
    seznam1.append(i[1:-4])

ime2 = re.findall(r'<TD(.*?)</TD>', open('podatki.txt', 'r', encoding='cp1250').read())
seznam2 = []
for j in ime2:
    if ime2.index(j) % 4 == 0:
        seznam2.append(j)
seznam2 = seznam2[4:]
seznam3 = []
for h in seznam2:
    if seznam2.index(h) % 3 == 0:
        seznam3.append(h)
seznam4 = []
for n in seznam3:
    if ">" in n:
        n = n[1:]
    seznam4.append(n)
seznam5 = []
for m in seznam4:
    if ("ZAKLADNE" not in m) or ("KOMERCIALNI ZAPIS" not in m) or ("SIJ" not in m):
        seznam5.append(m)
seznam6 = []
for l in seznam5:
    if 'class' in l:
        l = l[22:]
    seznam6.append(l)
        
slovar = dict(zip(seznam6, seznam1))




def pretvori_podatke(niz):
    tecaj = (r'<TD vAlign=top>Tečaj</TD>\s<TD vAlign=(.*?) align=right>(.*?)</TD></TR>')
    sprem_tecaja_v_zadnjem_dnevu = (r'(<TD class=ozadjeTecajnica vAlign=top>Sprememba tečaja v zadnjem dnevu</TD>\s<TD class=ozadjeTecajnica vAlign=top align=right>(.*?)%</TD></TR>)')
    dnevni_promet = (r'(<TD vAlign=top>Dnevni promet (.*?)</TD>\s<TD vAlign=top align=right>(.*?) </TD></TR>)')
    trzna_kapitalizacija = (r'(<TD class=ozadjeTecajnica vAlign=top>Tržna kapitalizacija (.*?) </TD>\s<TD class=ozadjeTecajnica vAlign=top align=right>(.*?) </TD></TR>)')
    razpon_cene = (r'<TD vAlign=top>52-tedenski razpon cene</TD>\s<TD vAlign=top align=right>(.*?)<BR>(.*?)</TD>')
    dividenda = (r'<TD class=ozadjeTecajnica vAlign=top align=right>(.*?)<!--(.*?)--></TD>(.*?)</TBODY></TABLE><BR>')
    dividendna_donostnost = (r'<TD class=ozadjeTecajnica vAlign=top>Dividendna donosnost</TD>\s<TD class=ozadjeTecajnica vAlign=top align=right>(.*?)</TD>')
    kapitalski_donos = (r'<TD vAlign=top>Kapitalski donos v zadnjih 52 tednih</TD>\s<TD vAlign=top align=right>(.*?)</TD>')

    rez1 = re.search(tecaj, niz).group(2)
    rez2 = re.search(sprem_tecaja_v_zadnjem_dnevu, niz).group(2)
    rez3 = re.search(dnevni_promet, niz).group(3)
    rez4 = re.search(trzna_kapitalizacija, niz).group(3)
    rez5 = re.search(razpon_cene, niz).group(1)
    rez6 = re.search(razpon_cene, niz).group(2)
    rez7 = re.search(dividenda, niz).group(1)
    rez8 = re.search(dividendna_donostnost, niz).group(1)
    rez9 = re.search(kapitalski_donos, niz).group(1)

    rez1 = rez1.replace(",", ".")
    rez2 = rez2.replace(",", ".")
    rez3 = rez3.replace(",", ".")
    rez4 = rez4.replace(",", ".")
    rez5 = rez5.replace(",", ".")
    rez6 = rez6.replace(",", ".")
    rez7 = rez7.replace(",", ".")
    rez8 = rez8.replace(",", ".")
    rez9 = rez9.replace(",", ".")
     
    
    return(rez1, rez2, rez3, rez4, rez5, rez6, rez7, rez8, rez9)


class Borza():


    def papir(self, *argumenti):
        vredpapir = self.var1.get()
        if vredpapir in slovar.keys():
            url2 = 'http://www.ljse.si/cgi-bin/jve.cgi?SecurityID='+slovar[vredpapir]+'&doc=818'
            req2 = urllib.request.Request(url2)
            response2 = urllib.request.urlopen(req2)
            the_page2 = response2.read().decode('cp1250')

            with open('vrednostni_papir.txt', 'w', encoding='cp1250') as f:
                print(the_page2, file=f)
            with open("vrednostni_papir.txt", "r", encoding='cp1250') as h:
                vrednostni_papir = h.readlines()
                pravi = ""
                for i in vrednostni_papir:
                    if i != "\n":
                        pravi = pravi+i
                podatki = pretvori_podatke(pravi)

                lol = abs((float(podatki[4])-float(podatki[5]))/3)
                if float(podatki[0]) < float(podatki[4])+lol:
                    stori = 'Kupi!'
                elif float(podatki[0]) > float(podatki[4])+2*lol:
                    stori = 'Prodaj!'
                else:
                    stori = 'Počakaj.'

                self.te.set(podatki[0])
                self.szd.set(podatki[1])
                self.dp.set(podatki[2])
                self.tk.set(podatki[3])
                self.rc1.set(podatki[4])
                self.rc2.set(podatki[5])
                self.di.set(podatki[6])
                self.dd.set(podatki[7])
                self.kd.set(podatki[8])
                self.ss.set(stori)
                


    def __init__(self, root):
        root.title("Vrednostni papirji")
        okvir = Frame(root, padx=30, pady=30)
        okvir.grid(column=0, row=0)
        self.var1 = StringVar()
        self.te = StringVar()
        self.szd = StringVar()
        self.dp = StringVar()
        self.tk = StringVar()
        self.rc1 = StringVar()
        self.rc2 = StringVar()
        self.di = StringVar()
        self.dd = StringVar()
        self.kd = StringVar()
        self.ss = StringVar()
        vnosno_polje = Entry(okvir, textvariable=self.var1)
        vnosno_polje.grid(column=2, row=1)
        Label(okvir, text="Vrednostni papir").grid(column=1, row=1)
        Button(okvir, text="Poišči!", command=self.papir).grid(column=2, row=2)
        Label(okvir, text='Tečaj').grid(column=1, row=3)
        Label(okvir, text='Sprem. v zadnjem dnevu').grid(column=1, row=4)
        Label(okvir, text='Dnevni promet (v 1000)').grid(column=1, row=5)
        Label(okvir, text='Tržna kapitalizacija (v mio)').grid(column=1, row=6)
        Label(okvir, text='52-tedenski razpon cene').grid(column=1, row=7)
        Label(okvir, text='Dividenda').grid(column=1, row=8)
        Label(okvir, text='Dividendna donosnost').grid(column=1, row=9)
        Label(okvir, text='Kapitalski donos v zadnjih 52 tednih').grid(column=1, row=10)
        Label(okvir, text='Kaj naj storim?').grid(column=1, row=11)
        Label(okvir, text='Eur').grid(column=4, row=3)
        Label(okvir, text='%').grid(column=4, row=4)
        Label(okvir, text='Eur').grid(column=4, row=5)
        Label(okvir, text='Eur').grid(column=4, row=6)
        Label(okvir, text='Eur').grid(column=4, row=7)
        Label(okvir, text='Eur').grid(column=4, row=8)
        Label(okvir, text='%').grid(column=4, row=9)
        Label(okvir, text='%').grid(column=4, row=10)
        Label(okvir, textvariable=self.te).grid(column=2, row=3)
        Label(okvir, textvariable=self.szd).grid(column=2, row=4)
        Label(okvir, textvariable=self.dp).grid(column=2, row=5)
        Label(okvir, textvariable=self.tk).grid(column=2, row=6)
        Label(okvir, textvariable=self.rc1).grid(column=2, row=7)
        Label(okvir, textvariable=self.rc2).grid(column=3, row=7)
        Label(okvir, textvariable=self.di).grid(column=2, row=8)
        Label(okvir, textvariable=self.dd).grid(column=2, row=9)
        Label(okvir, textvariable=self.kd).grid(column=2, row=10)
        Label(okvir, textvariable=self.ss).grid(column=2, row=11)
        root.bind("<Return>", self.papir)
        vnosno_polje.focus()



root = Tk()
app = Borza(root)
root.mainloop()






